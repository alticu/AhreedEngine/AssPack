#ifndef LZ4_STREAM
#define LZ4_STREAM

// LZ4 Headers
#include <lz4frame.h>

// Standard headers
#include <array>
#include <cassert>
#include <iostream>
#include <memory>
#include <streambuf>
#include <vector>

/**
 * @brief An input stream that will LZ4 decompress output data.
 *
 * An input stream that will wrap another input stream and LZ4
 * decompress its output data to that stream.
 *
 */
class LZ4InputStream : public std::istream {
   public:
    /**
     * @brief Constructs an LZ4 decompression input stream
     *
     * @param source The stream to read LZ4 compressed data from
     */
    LZ4InputStream(std::istream& source)
        : std::istream(new LZ4InputBuffer(source)),
          buffer_(dynamic_cast<LZ4InputBuffer*>(rdbuf())) {
        assert(buffer_);
    }

    /**
     * @brief Destroys the LZ4 output stream.
     */
    ~LZ4InputStream() { delete buffer_; }

   private:
    class LZ4InputBuffer : public std::streambuf {
       public:
        LZ4InputBuffer(std::istream& source);
        ~LZ4InputBuffer();
        int_type underflow() override;

        LZ4InputBuffer(const LZ4InputBuffer&) = delete;
        LZ4InputBuffer& operator=(const LZ4InputBuffer&) = delete;

       private:
        std::istream& source_;
        std::array<char, 64 * 1024> src_buf_;
        std::array<char, 64 * 1024> dest_buf_;
        size_t offset_;
        size_t src_buf_size_;
        LZ4F_decompressionContext_t ctx_;
    };

    LZ4InputBuffer* buffer_;
};

#endif  // LZ4_STREAM
