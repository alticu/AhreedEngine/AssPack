#include "AssPack.h"
#include "substream/substream.h"

#include <lz4.h>
#include <cstring>
#include <iostream>

using namespace AssPack;

AssArchive::AssArchive(const std::string &filename)
    : _stream(filename, std::ios_base::in | std::ios_base::binary) {
    _stream.read((char *)&_header, sizeof(PackHeader));

    if (_header.magic != 21770)
        throw std::runtime_error("File is not an AssPack archive");

    FileHeader h;
    for (int i = 0; i < _header.fileCount; i++) {
        _stream.read((char *)&h, sizeof(FileHeader));

        std::string name(h.name);
        _files[name] = h;

        std::ifstream tf(filename);
        substreambuf *s = new substreambuf(tf.rdbuf(), h.offset, h.cSize);
        std::istream *is = new std::istream(s);
        _fileStreams[name] = new LZ4InputStream(*is);
    }

    FileHeader f = _files["CMakeFiles/cmake.check_cache"];
    LZ4InputStream *s = _fileStreams["CMakeFiles/cmake.check_cache"];

    char outBuf[f.size];
    s->read(outBuf, f.size);

    std::ofstream osub("/tmp/test.txt",
                       std::ios_base::out | std::ios_base::binary);
    osub.write(outBuf, f.size);
    osub.close();
}

int main() {
    AssArchive a("foo.pak");
    return 0;
}
