#ifndef ASSPACK_H
#define ASSPACK_H

#include <lz4_stream.h>
#include <cstdint>
#include <fstream>
#include <string>
#include <unordered_map>

namespace AssPack {

typedef struct {
    uint16_t magic;
    uint16_t fileCount;
} PackHeader;

typedef struct {
    uint64_t offset;
    uint64_t size;
    uint64_t cSize;
    char name[128];
} FileHeader;

class AssArchive {
   public:
    AssArchive(const std::string &filename);

   private:
    std::ifstream _stream;
    PackHeader _header;
    std::unordered_map<std::string, FileHeader> _files;
    std::unordered_map<std::string, LZ4InputStream *> _fileStreams;
};

};  // namespace AssPack

#endif
