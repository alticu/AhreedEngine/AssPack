#ifndef SUBSTREAM_H
#define SUBSTREAM_H

#include <streambuf>

class substreambuf : public std::streambuf {
   public:
    substreambuf(std::streambuf *sbuf, std::size_t start, std::size_t len);

   protected:
    int underflow();
    int uflow();
    std::streampos seekoff(std::streamoff off, std::ios_base::seekdir way,
                           std::ios_base::openmode which = std::ios_base::in |
                                                           std::ios_base::out);
    std::streampos seekpos(std::streampos sp,
                           std::ios_base::openmode which = std::ios_base::in |
                                                           std::ios_base::out);

   private:
    std::streambuf *m_sbuf;
    std::streampos m_start;
    std::streampos m_len;
    std::streampos m_pos;
};

#endif
