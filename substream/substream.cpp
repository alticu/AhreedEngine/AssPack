#include "substream.h"

#include <cassert>

// Adapted from https://stackoverflow.com/a/7657367

substreambuf::substreambuf(std::streambuf *sbuf, std::size_t start,
                           std::size_t len)
    : m_sbuf(sbuf), m_start(start), m_len(len), m_pos(0) {
    std::streampos p = m_sbuf->pubseekpos(start);
    assert(p != std::streampos(-1));
    setbuf(NULL, 0);
}

int substreambuf::underflow() {
    if (m_pos + std::streamsize(1) >= m_len) return traits_type::eof();
    return m_sbuf->sgetc();
}

int substreambuf::uflow() {
    if (m_pos + std::streamsize(1) > m_len) return traits_type::eof();
    m_pos += std::streamsize(1);
    return m_sbuf->sbumpc();
}

std::streampos substreambuf::seekoff(std::streamoff off,
                                     std::ios_base::seekdir way,
                                     std::ios_base::openmode which) {
    std::streampos cursor;

    if (way == std::ios_base::beg)
        cursor = off;
    else if (way == std::ios_base::cur)
        cursor = m_pos + off;
    else if (way == std::ios_base::end)
        cursor = m_len - off;

    if (cursor < 0 || cursor >= m_len) return std::streampos(-1);
    m_pos = cursor;
    if (m_sbuf->pubseekpos(m_start + m_pos, which) == std::streampos(-1))
        return std::streampos(-1);

    return m_pos;
}

std::streampos substreambuf::seekpos(std::streampos sp,
                                     std::ios_base::openmode which) {
    if (sp < 0 || sp >= m_len) return std::streampos(-1);
    m_pos = sp;
    if (m_sbuf->pubseekpos(m_start + m_pos, which) == std::streampos(-1))
        return std::streampos(-1);
    return m_pos;
}
